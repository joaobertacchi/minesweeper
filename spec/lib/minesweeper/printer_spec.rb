require 'spec_helper'
require 'minesweeper/printer'

describe Minesweeper::SimplePrinter do
  before :each do
    @board_state = [
      [:bomb, :unknown_cell, 3, :flag, :clear_cell],
      [4, 4, 4, 4, 4]
    ]
  end

  it 'has SimplePrinter class' do
    expect(Minesweeper::SimplePrinter.new).not_to eq(nil)
  end

  describe '#print' do
    it 'all states' do
      expect do
        Minesweeper::SimplePrinter.new(true).print(@board_state)
      end.to output("# . 3 F  \n4 4 4 4 4\n").to_stdout
    end
  end
end

describe Minesweeper::PrettyPrinter do
  it 'has PrettyPrinter class' do
    expect(Minesweeper::PrettyPrinter.new).not_to eq(nil)
  end
  describe '#print' do
    [true, false].each do |use_default_board_color_and_format|
      context "single cell board with #{(use_default_board_color_and_format ? 'default' : 'preset')} board_format" do
        states = [:bomb, :flag, :unknown_cell, :clear_cell, 3]
        preset_format = {
          unknown_cell: '.',
          clear_cell: ' ',
          bomb: '#',
          flag: 'F'
        }
        preset_colors = {
          unknown_cell: :red,
          clear_cell: :green,
          bomb: :blue,
          flag: :white,
          numeric_cell: :magenta
        }

        default_format = {
          unknown_cell: '.',
          clear_cell: ' ',
          bomb: 'o',
          flag: 'P'
        }
        default_colors = {
          unknown_cell: nil,
          clear_cell: nil,
          bomb: :light_red,
          flag: :light_yellow,
          numeric_cell: :light_white
        }
        if use_default_board_color_and_format
          printer = Minesweeper::PrettyPrinter.new(false)
          expected_format = default_format
          expected_colors = default_colors
        else
          printer = Minesweeper::PrettyPrinter.new(false, preset_format)
          expected_format = preset_format
          expected_colors = preset_colors
          expected_colors.each do |state, color|
            printer.set_cell_state_color(state, color)
          end
        end

        states.each do |state|
          color = expected_colors[((state.is_a? Numeric) ? :numeric_cell : state)]
          it "where state is #{state} and color is #{color}" do
            board_state = [[state]]

            expect do
              printer.print(board_state)
            end.to output(frame_single_character(state_to_character(state, expected_format), color)).to_stdout
          end
        end
      end
    end
    it 'single numeric cell' do
      board_state = [[1]]
      color = :light_white
      printer = Minesweeper::PrettyPrinter.new
      printer.set_cell_state_color(:numeric_cell, color)
      expect do
        printer.print(board_state)
      end.to output(frame_single_character('1', color)).to_stdout
    end
    it 'single bomb' do
      board_state = [[:bomb]]
      expect do
        Minesweeper::PrettyPrinter.new.print(board_state)
      end.to output(frame_single_character('o', :light_red)).to_stdout
    end
    it 'single flag' do
      board_state = [[:flag]]
      expect do
        Minesweeper::PrettyPrinter.new.print(board_state)
      end.to output(frame_single_character('P', :light_yellow)).to_stdout
    end
    it 'single closed cell' do
      board_state = [[:unknown_cell]]
      expect do
        Minesweeper::PrettyPrinter.new.print(board_state)
      end.to output(frame_single_character('.', nil)).to_stdout
    end
    it 'single clear cell' do
      board_state = [[:clear_cell]]
      expect do
        Minesweeper::PrettyPrinter.new.print(board_state)
      end.to output(frame_single_character(' ', nil)).to_stdout
    end
  end
  describe "#cell_state_color" do
    states = [3, :flag]
    states.each do |state|
      it "assigns a color to a cell with state #{state}" do
        printer = Minesweeper::PrettyPrinter.new
        color = :red

        printer.set_cell_state_color(state, color)

        expect(printer.get_cell_state_color(state)).to eq(color)
      end
    end
  end

  private
  def frame_single_character(character, state_color, frame_color=:light_black)
    '      0|'.colorize(frame_color) + "\n" + 
    '    ---|'.colorize(frame_color) + "\n" +
    '  0|'.colorize(frame_color) + ' ' + character.colorize(state_color) + ' ' + '|'.colorize(frame_color) + "\n" +
    '   |---|'.colorize(frame_color) + "\n"
  end

  def state_to_character(state, board_format)
    return board_format[state] if board_format.key? state
    state.to_s
  end
end
