require 'contracts'

module Minesweeper
  # Store row and column cell position
  CellPosition = Struct.new(:row, :col)

  # Store the game board state
  class Board
    include Contracts::Core
    include Contracts::Builtin
    C = Contracts

    attr_reader :width, :height, :num_mines

    # def initialize(width, height, num_mines, bombs_position = nil)
    #   init(width, height, num_mines, bombs_position)
    # end

    # @param width is a positive integer
    # @param height is a positive integer
    # @param num_mines is a non-negative integer. Must be less than or equal to width * height
    # @param bombs_position is an matrix of height X width dimensions. 1 indicates bomb and 0, no-bomb.
    Contract C::Pos, C::Pos, C::Nat, C::Maybe[C::ArrayOf[C::ArrayOf[C::Nat]]] => C::Any
    def init(width, height, num_mines, bombs_position = nil)
      raise 'num_mines should be <= width*height' if num_mines > width * height
      @width = width
      @height = height
      @num_mines = num_mines
      @exploded = false

      @current_num_non_mines = @width * @height - @num_mines

      init_board(bombs_position)
      set_neighbor_bombs

      actual_num_mines = count_bombs
      actual_height = @board_cells.size
      actual_width = @board_cells[0].size
      raise "num_mines=#{@num_mines} and actual number of mines in bombs_position (#{actual_num_mines}) does not match" if actual_num_mines != @num_mines
      raise "height=#{@height} and actual height in bombs_position (#{actual_height}) does not match" if actual_height != @height
      raise "width=#{@width} and actual width in bombs_position (#{actual_width}) does not match" if actual_width != @width
    end

    # Indicates if any bomb in the board has exploded.
    # @return true if any bomb in the board has exploded. False otherwise.
    Contract C::None => C::Bool
    def exploded?
      @exploded
    end

    # Used for knowing if the game is finished.
    # @return the amount of closed cells that has no bomb.
    Contract C::None => C::Nat
    def closed_cells_without_bomb
      @current_num_non_mines
    end

    # Param config is ignored unless the game is over. In this case, closed cells
    # with bombs (with or without flag) return :bomb state.
    # @param config optional hash to enable xray feature. Works only if the game is finished
    # @return the current board representation as a height X width matrix.
    Contract C::Maybe[C::KeywordArgs[xray: C::Bool]] => C::ArrayOf[C::ArrayOf[Or[Symbol, C::Pos]]]
    def board_state(config = {})
      xray = false
      xray = config[:xray] if config.key?(:xray)

      @board_cells.map do |line|
        line.map { |cell| cell.state(xray) }
      end
    end

    def storage_state
      @board_cells.map do |line|
        line.map do |cell|
          cell.storage_state
        end
      end
    end

    def storage_state=(state)
      @board_cells = state.map do |line|
        line.map do |cell_state|
          Cell.new(*cell_state)
        end
      end
    end

    # Tries to open the cell. If it is clear, its neighbors are also opened.
    # @return true if the cell could be open.
    Contract C::Nat, C::Nat => C::Bool
    def expand(row, col)
      fist_cell_position = CellPosition.new(row, col)
      valid = play_and_track_non_mines_and_explosion(fist_cell_position)
      return false unless valid
      return valid if @exploded

      expand_cell_positions = [fist_cell_position]
      loop do
        expand_cell_position = expand_cell_positions.shift
        if position_is_clear? expand_cell_position
          neighbor_positions = closed_neighbor_coordinates_without_bomb(expand_cell_position)
          neighbor_positions.each do |neighbor_position|
            play_and_track_non_mines_and_explosion(neighbor_position)
            expand_cell_positions.push(neighbor_position)
          end
        end

        break if expand_cell_positions.empty?
      end

      valid
    end

    # Toggles the flag status. An open cell cannot be flagged.
    # @return true if flag status could be toggled. False otherwise.
    Contract C::Nat, C::Nat => C::Bool
    def toggle_flag(row, col)
      @board_cells[row][col].toggle_flag
    end

    private

    def position_is_clear?(cell_position)
      cell = @board_cells[cell_position.row][cell_position.col]
      cell.state == :clear_cell
    end

    def play_and_track_non_mines_and_explosion(cell_position)
      row = cell_position.row
      col = cell_position.col
      is_valid = @board_cells[row][col].play

      if is_valid
        @current_num_non_mines -= 1 unless @board_cells[row][col].bomb?
        @exploded ||= @board_cells[row][col].bomb?
      end

      is_valid
    end

    # @private
    def count_bombs
      bombs = 0
      @board_cells.each do |line|
        line.each do |cell|
          bombs += 1 if cell.bomb?
        end
      end
      bombs
    end

    def init_board(bombs_position)
      bombs_position = random_bombs_position if bombs_position.nil?

      @board_cells = bombs_position.map do |line|
        line.map do |bomb|
          has_bomb = (bomb == 1)
          Cell.new(has_bomb)
        end
      end
    end

    # This is a private method. It is being displayed here for it to be linkable from
    # Requirements Verification section in README.md
    #
    # A matrix of height X width dimenstions with 0s and 1s is created. Zero means no bomb
    # and One means bomb. This matrix has exactly num_mines 1s randomly distributed. This
    # matrix is used by the boad constructor {Minesweeper::Cell#initialize} for creating
    # objects with and without bombs
    #
    # @!visibility public
    def random_bombs_position
      initial_num_non_mines = @width * @height - @num_mines
      shuffled_bombs = (Array.new(@num_mines) { 1 } + Array.new(initial_num_non_mines) { 0 }).shuffle
      shuffled_bombs.each_slice(@width).to_a
    end

    def set_neighbor_bombs
      (0..(@height - 1)).each do |i|
        (0..(@width - 1)).each do |j|
          increment_neighbors(i, j) if @board_cells[i][j].bomb?
        end
      end
    end

    def increment_neighbors(x, y)
      ((x - 1)..(x + 1)).each do |i|
        next unless (i >= 0) && (i < @height)
        ((y - 1)..(y + 1)).each do |j|
          if (j >= 0) && (j < @width)
            @board_cells[i][j].neighbor_bombs += 1 if (i != x) || (j != y)
          end
        end
      end
    end

    def neighbor_coordinates(cell_position)
      row_interval = ((cell_position.row - 1)..(cell_position.row + 1))
      col_interval = ((cell_position.col - 1)..(cell_position.col + 1))

      neighbors = []

      row_interval.each do |row|
        col_interval.each do |col|
          neighbor_position = CellPosition.new(row, col)
          neighbors.push(neighbor_position) if neighbor_position_is_valid?(neighbor_position, cell_position)
        end
      end
      neighbors
    end

    def neighbor_position_is_valid?(neighbor_position, cell_position)
      nrow = neighbor_position.row
      ncol = neighbor_position.col
      crow = cell_position.row
      ccol = cell_position.col
      row_is_valid?(nrow) and col_is_valid?(ncol) and ((nrow != crow) || (ncol != ccol))
    end

    def row_is_valid?(row)
      (row >= 0) && (row < @height)
    end

    def col_is_valid?(col)
      (col >= 0) && (col < @width)
    end

    def closed_neighbor_coordinates_without_bomb(cell_position)
      neighbor_coordinates(cell_position).select do |neighbor_position|
        position_is_closed_and_has_no_bomb_and_has_no_flag?(neighbor_position)
      end
    end

    def position_is_closed_and_has_no_bomb_and_has_no_flag?(position)
      cell = @board_cells[position.row][position.col]
      !cell.open? && !cell.bomb? && !cell.flag?
    end
  end
end
